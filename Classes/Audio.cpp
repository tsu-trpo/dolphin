#include "Audio.h"
#include <string>
#include <SimpleAudioEngine.h>
#include <cocos2d.h>
#include "Constants.h"

using namespace CocosDenshion;

USING_NS_CC;

auto audio = SimpleAudioEngine::getInstance();

void Audio::playMusic()
{
    audio->stopBackgroundMusic();
    audio->playBackgroundMusic(file::audio::musicbackground, true);
}

void Audio::pauseMusic()
{
    audio->pauseBackgroundMusic();
}

void Audio::resumeMusic()
{
    audio->resumeBackgroundMusic();
}
