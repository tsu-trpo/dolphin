#pragma once

#include <cocos2d.h>
#include "Hero.h"

using KeyCode = cocos2d::EventKeyboard::KeyCode;

class HeroController : public cocos2d::Node {
public:
    static HeroController *create(Hero *hero);

private:
    KeyCode onKeyboardPressed(KeyCode keyCode, cocos2d::Event *event);
    Hero *hero;
};
