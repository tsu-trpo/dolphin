#pragma once

class Audio {
public:
    static void playMusic();
    static void pauseMusic();
    static void resumeMusic();
};