#pragma once

#include <ui/CocosGUI.h>
#include "cocos2d.h"

USING_NS_CC;

class AudioButton : public cocos2d::Node {
public:
    explicit AudioButton(Node &audioButton);
    static AudioButton *create(Node &button);
    void touch(Ref *sender, ui::Widget::TouchEventType type);

private:
    ui::Button *sound;
    bool isSoundOn = true;
};
