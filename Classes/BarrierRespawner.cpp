#include "BarrierRespawner.h"
#include "Constants.h"
#include "ScreenConsts.h"

Vec2 spawnUpPosition()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    const float relativeHeight = 0.75;
    return Vec2(origin.x + visibleSize.width, origin.y + visibleSize.height * relativeHeight);
}

Vec2 spawnDownPosition()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return Vec2(origin.x + visibleSize.width, origin.y);
}

BarrierRespawner::BarrierRespawner(Node &spawnPoint)
    : currentTime{0}
    , spawnPoint{spawnPoint}
{
    scheduleUpdate();
}

BarrierRespawner *BarrierRespawner::create(Node &spawnPoint)
{
    BarrierRespawner *respawner = new BarrierRespawner(spawnPoint);
    respawner->autorelease();
    return respawner;
}

Barrier *BarrierRespawner::spawnBarrier(const std::string &texture)
{
    return Barrier::create(texture);
}

void BarrierRespawner::spawnBuoy()
{
    Barrier *buoy = spawnBarrier(file::texture::buoy);
    Vec2 pos = spawnUpPosition();
    buoy->setPosition(pos);
    spawnPoint.addChild(buoy);
}

void BarrierRespawner::spawnAlga()
{
    Barrier *alga = spawnBarrier(file::texture::alga);
    Vec2 pos = spawnDownPosition() + Vec2{0, alga->getSize().height / 2};
    alga->setPosition(pos);
    spawnPoint.addChild(alga);
}

void BarrierRespawner::update(float delta)
{
    currentTime += delta;

    if (currentTime <= timeBetweenBarriers)
        return;

    currentTime = 0;
    auto random = CCRANDOM_0_1();

    // с вероятностью 1/3 создаём буёк
    // с вероятностью 1/3 создаём водоросли
    // с вероятностью 1/3 создаём и буёк, и водоросли
    //
    if (random < 0.66f) {
        spawnBuoy();
    }
    if (random > 0.33f) {
        spawnAlga();
    }
}
