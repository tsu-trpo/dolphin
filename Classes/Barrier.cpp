#include "Barrier.h"
#include "Constants.h"
#include "Events.h"
#include "PhysicTags.h"

Barrier::Barrier(const std::string &name)
{
    scheduleUpdate();

    sprite = Sprite::create(name);
    auto spriteBody = cocos2d::PhysicsBody::createBox(sprite->getContentSize());
    spriteBody->setContactTestBitmask(0xFFFFFFFF);
    spriteBody->setDynamic(false);
    sprite->setPhysicsBody(spriteBody);
    sprite->setName(physic_tags::barrier);
    
    sprite->setScale(0.25);
    addChild(sprite, 0);
}

Barrier *Barrier::create(const std::string &name)
{
    Barrier *barrier = new Barrier(name);
    barrier->autorelease();

    return barrier;
}

Size Barrier::getSize()
{
    return sprite->getContentSize() * sprite->getScale();
}

void Barrier::update(float delta)
{
    Point currentPosition = getPosition();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size barrierSize = getSize();

    bool isOutOfScreen = currentPosition.x < origin.x - barrierSize.width;
    if (!isOutOfScreen) {
        const int offset = -delta * velocity;

        setPositionX(currentPosition.x + offset);
    } else {
        runAction(RemoveSelf::create());
    }

    return;
}
