#include "AudioButton.h"
#include "Audio.h"
#include "Constants.h"

AudioButton *AudioButton::create(Node &button)
{
    AudioButton *audioButton = new AudioButton(button);
    audioButton->autorelease();
    return audioButton;
}

AudioButton::AudioButton(Node &audioButton)
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    sound = ui::Button::create(file::texture::audioYes, file::texture::audioYes);
    sound->setPosition(Vec2(visibleSize.width * 0.97 + origin.x, visibleSize.height * 0.96));
    addChild(sound, 2);
    sound->addTouchEventListener(CC_CALLBACK_2(AudioButton::touch, this));
}

void AudioButton::touch(Ref *sender, ui::Widget::TouchEventType type)
{
    switch (type) {
    case ui::Widget::TouchEventType::ENDED:
        if (isSoundOn) {
            sound->loadTextures(file::texture::audioNo, file::texture::audioNo);
            isSoundOn = false;
            Audio::pauseMusic();
        } else {
            sound->loadTextures(file::texture::audioYes, file::texture::audioYes);
            isSoundOn = true;
            Audio::resumeMusic();
        }
        break;
    default:
        break;
    }
}
