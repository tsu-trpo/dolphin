#include "HeroController.h"
#include "Audio.h"
#include "Events.h"

HeroController *HeroController::create(Hero *hero)
{
    auto self = new HeroController();
    self->autorelease();

    // подписка на события о нажатии клавиш
    auto listener = cocos2d::EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(HeroController::onKeyboardPressed, self);
    self->_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, self);

    self->hero = hero;

    return self;
}

KeyCode HeroController::onKeyboardPressed(KeyCode keyCode, cocos2d::Event *event)
{
    switch (keyCode) {
    case KeyCode::KEY_W:
    case KeyCode::KEY_UP_ARROW:
    case KeyCode::KEY_SPACE:
        hero->jump();
    default:
        break;
    }
    return keyCode;
}
