#include "GameController.h"
#include "Events.h"
#include "MenuScene.h"

GameController * GameController::create()
{
    auto self = new GameController();
    self->autorelease();
    self->addEventListener();
    return self;
}


void GameController::addEventListener()
{
    getEventDispatcher()->addCustomEventListener(events::wasContacted, [&](EventCustom *event) { 
        auto scene = MenuScene::create();
        Director::getInstance()->replaceScene(scene); 
    });
}

GameController::~GameController()
{
    getEventDispatcher()->removeCustomEventListeners(events::wasContacted);
}
