#include "DolphinSelectors.h"
#include "Constants.h"
#include "Settings.h"

USING_NS_CC;

void selectDolphinBlack()
{
    Settings::dolphinSprite = file::texture::dolphinBlack;
}

void selectDolphinWhite()
{
    Settings::dolphinSprite = file::texture::dolphinWhite;
}

void selectDolphinBlue()
{
    Settings::dolphinSprite = file::texture::dolphinBlue;
}