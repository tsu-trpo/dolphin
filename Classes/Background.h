#pragma once

#include "cocos2d.h"

USING_NS_CC;

class Background : public Node {
public:
    explicit Background(int number);
    static Background *create(int number);
    void update(float delta);
    Size getSize();

    const static int overlap = -3;

private:
    const int velocity = 200;
    Sprite *sprite = nullptr;
};
