#pragma once

#include "cocos2d.h"

USING_NS_CC;

class Barrier : public Node {
public:
    void update(float delta);
    Size getSize();
    Barrier(const std::string &name);
    static Barrier *create(const std::string &name);
    
protected:
    Sprite *sprite = nullptr;
    const int velocity = 500;
};
