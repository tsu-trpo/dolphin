#pragma once

namespace events {

const char *const barrierIsPassed = "barrierIsPassed";
const char *const wasContacted = "wasContacted";
}
