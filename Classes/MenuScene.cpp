#include "MenuScene.h"
#include "Constants.h"
#include "GameScene.h"
#include "SelectorScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene *MenuScene::createScene()
{
    return MenuScene::create();
}

bool MenuScene::init()
{
    if (!Scene::init()) {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto startItem =
        MenuItemImage::create(file::menu::play, file::menu::play_2, CC_CALLBACK_1(MenuScene::goToSelectorScene, this));
    startItem->setPosition(Vec2{origin.x + visibleSize.width - startItem->getContentSize().width / 2 - 50,
                                origin.y + startItem->getContentSize().height / 2 + 220});

    auto menu = Menu::create(startItem, NULL);
    menu->setPosition(Vec2::ZERO);
    addChild(menu, 1);

    auto closeItem =
        MenuItemImage::create(file::menu::exit, file::menu::exit_2, CC_CALLBACK_1(MenuScene::menuCloseCallback, this));
    closeItem->setPosition(Vec2{origin.x + visibleSize.width - closeItem->getContentSize().width / 2 - 50,
                                origin.y + closeItem->getContentSize().height / 2 + 120});

    menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    addChild(menu, 1);

    auto background = Sprite::create(file::menu::menu);
    background->setPosition(getCenter());
    addChild(background, 0);

    auto label = Label::createWithTTF("D O L P H I N", file::font::ubuntu_bold, 50);
    label->setPosition(
        Vec2{origin.x + visibleSize.width / 2, origin.y + visibleSize.height - label->getContentSize().height});
    addChild(label, 1);

    auto sprite = Sprite::create(file::menu::dolphin);
    sprite->setPosition(getCenter());
    addChild(sprite, 0);

    return true;
}

void MenuScene::menuCloseCallback(Ref *pSender)
{
    Director::getInstance()->end();
}

void MenuScene::goToSelectorScene(Ref *sender)
{
    const float transitionTime = 0.5;
    auto scene = SelectorScene::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(transitionTime, scene));
}
