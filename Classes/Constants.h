#pragma once
#include <string>

namespace file {

namespace font {
const std::string arial = "fonts/arial.ttf";
const std::string marker_felt = "fonts/Marker_Felt.ttf";
const std::string ubuntu_bold = "fonts/ubuntu_bold.ttf";
}  // namespace font

namespace audio {
const std::string musicbackground = "music/sea.mp3";
}  // namespace audio

namespace texture {
const std::string audioNo = "music/audioNo.png";
const std::string audioYes = "music/audioYes.png";
const std::string dolphinBlue = "res/dolphinBlue.png";
const std::string dolphinBlack = "res/dolphinBlack.png";
const std::string dolphinWhite = "res/dolphinWhite.png";
const std::string buoy = "buoy.png";
const std::string alga = "alga.png";

inline std::string getBackground(int number)
{
    return "res/backgraund" + std::to_string(number) + ".png";
}
}  // namespace texture

namespace menu {
const std::string menu = "menu.png";
const std::string play = "play_games.png";
const std::string play_2 = "play_game_click.png";
const std::string exit = "exit.png";
const std::string exit_2 = "exit_click.png";
const std::string dolphin = "dol1.png";
}  // namespace menu

}  // namespace file
