#pragma once
#include <cocos2d.h>

USING_NS_CC;

inline Vec2 getLeftUpCorner()
{
    Vec2 visibleOrigin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return Vec2(visibleOrigin.x, visibleOrigin.y + visibleSize.height);
}

inline Vec2 getCenter()
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    return Vec2{origin.x + visibleSize.width / 2.0f, origin.y + visibleSize.height / 2.0f};
}

inline bool isOutOfScreen(const Vec2 &point)
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();

    return point.x < origin.x || point.x > visibleSize.width || point.y < origin.y || point.y > visibleSize.height;
}