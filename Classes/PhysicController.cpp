#include "PhysicController.h"
#include "Barrier.h"
#include "ContactHelper.h"
#include "GameController.h"
#include "MenuScene.h"
#include "PhysicTags.h"
#include "Events.h"
#include <string>

PhysicController *PhysicController::create()
{
    auto self = new PhysicController();
    self->autorelease();

    self->listener = cocos2d::EventListenerPhysicsContact::create();
    self->listener->onContactBegin = CC_CALLBACK_1(PhysicController::onContactBegin, self);
    self->getEventDispatcher()->addEventListenerWithSceneGraphPriority(self->listener, self);
    
    return self;
}

bool PhysicController::onContactBegin(cocos2d::PhysicsContact &contact)
{
    ContactHelper helper{contact, physic_tags::hero};
    if (!helper.wasContacted()) {
        return false;
    }
    getEventDispatcher()->dispatchCustomEvent(events::wasContacted);
    return true;
}
