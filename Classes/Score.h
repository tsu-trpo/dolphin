#pragma once

#include "cocos2d.h"

USING_NS_CC;

class Score : public Node {
public:
    Score();
    ~Score();
    static Score *create();
    void update(float delta);
    void addEventListener();

private:
    double score = 0;
    unsigned int scoreMultiplier = 1;
    Label *scoreLabel = nullptr;
};
