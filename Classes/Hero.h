#pragma once

#include "cocos2d.h"

USING_NS_CC;

class Hero : public Sprite {
public:
    Hero();
    static Hero *create();
    void update(float delta) override;
    void jump();

private:
    const float jumpForce = 5.f;
    const float gravity = -5.f;
    float velocityY = 0.f;
};
