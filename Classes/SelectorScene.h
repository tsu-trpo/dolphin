#pragma once

#include <functional>
#include <cocos2d.h>
USING_NS_CC;

class SelectorScene : public cocos2d::Scene {
public:
    static cocos2d::Scene *createScene();

    virtual bool init();

    CREATE_FUNC(SelectorScene);

private:
    void addButton(Vec2 shift, const std::string &dolphin, const std::function<void()>);
    void goToGameScene(cocos2d::Ref *sender);
};
