#pragma once

#include <cocos2d.h>

class GameController : public cocos2d::Node {
public:
    static GameController *create();
    ~GameController();
    void addEventListener();
};  
