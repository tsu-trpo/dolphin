#include "GameScene.h"
#include "Audio.h"
#include "AudioButton.h"
#include "BackgroundController.h"
#include "BarrierRespawner.h"
#include "Constants.h"
#include "Events.h"
#include "GameController.h"
#include "Hero.h"
#include "HeroController.h"
#include "MenuScene.h"
#include "PhysicController.h"
#include "Score.h"
#include "SimpleAudioEngine.h"

Scene *GameScene::createScene()
{
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

    auto layer = GameScene::create();
    layer->physicsWorld = scene->getPhysicsWorld();

    scene->addChild(layer);

    return scene;
}

bool GameScene::init()
{
    if (!Scene::init()) {
        return false;
    }

    AudioButton *audioButton = AudioButton::create(*this);
    addChild(audioButton, 1);

    BackgroundController *backgroundController = BackgroundController::create(*this);
    addChild(backgroundController, -1);

    BarrierRespawner *respawner = BarrierRespawner::create(*this);
    addChild(respawner, 0);

    Hero *player = Hero::create();
    addChild(player, 1);

    HeroController *heroController = HeroController::create(player);
    addChild(heroController);

    Score *score = Score::create();
    addChild(score, 0);
    
    auto physicController = PhysicController::create();
    addChild(physicController);
    
    auto gameController = GameController::create();
    addChild(gameController);

    Audio::playMusic();

    return true;
}
