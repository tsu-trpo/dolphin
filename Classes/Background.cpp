#include "Background.h"
#include "Constants.h"

Background::Background(int number)
{
    scheduleUpdate();
    sprite = Sprite::create(file::texture::getBackground(number));
    sprite->setAnchorPoint(Vec2(0, 0.5));
    addChild(sprite, -1);
}

Background *Background::create(int number)
{
    Background *background = new Background(number);
    background->autorelease();

    return background;
}

Size Background::getSize()
{
    Size backgroundSize = sprite->getContentSize();
    return backgroundSize;
}

void Background::update(float delta)
{
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    bool isOutOfScreen = getPosition().x + getSize().width < origin.x;

    if (!isOutOfScreen) {
        const int offset = -delta * velocity;
        setPositionX(getPosition().x + offset);
    } else {
        double screenOffset = getSize().width + overlap;
        setPositionX(getPosition().x + 2 * screenOffset);
    }
}
