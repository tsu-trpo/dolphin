#pragma once

#include <cocos2d.h>

class PhysicController : public cocos2d::Node {
public:
    static PhysicController *create();
    bool onContactBegin(cocos2d::PhysicsContact &contact);
    
private:
    cocos2d::EventListenerPhysicsContact *listener = nullptr;
}; 
