#pragma once

#include <string>
#include "Barrier.h"
#include "cocos2d.h"

USING_NS_CC;

class BarrierRespawner : public cocos2d::Node {
public:
    explicit BarrierRespawner(Node &spawnPoint);
    static BarrierRespawner *create(Node &spawnPoint);

    void update(float delta);

    Barrier *spawnBarrier(const std::string &);
    void spawnBuoy();
    void spawnAlga();

private:
    const float timeBetweenBarriers = 0.7;
    float currentTime = 0;
    Node &spawnPoint;
};
