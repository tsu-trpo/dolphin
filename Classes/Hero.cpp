#include "Hero.h"
#include "ScreenConsts.h"
#include "Settings.h"
#include "PhysicTags.h"

USING_NS_CC;

Hero::Hero()
{
    initWithFile(Settings::dolphinSprite);
    auto visibleSize = Director::getInstance()->getVisibleSize();
    setPosition(Vec2(visibleSize.width / 6, visibleSize.height / 2));
    setScale(0.8);
}

Hero *Hero::create()
{
    Hero *hero = new Hero();
    hero->autorelease();
    hero->scheduleUpdate();

    auto heroBody = cocos2d::PhysicsBody::createBox(hero->getContentSize());
    heroBody->setDynamic(false);
    heroBody->setContactTestBitmask(0xFFFFFFFF);
    heroBody->setName(physic_tags::hero);

    hero->setPhysicsBody(heroBody);
    return hero;
}

void Hero::update(float delta)
{
    velocityY += gravity * delta;
    setPositionY(getPositionY() + velocityY);

    if (isOutOfScreen(getPosition())) {
        auto pos = getPosition();
        cocos2d::Vec2 visibleSize = Director::getInstance()->getVisibleSize();
        setPositionY(((int) pos.y + (int) visibleSize.y) % (int) visibleSize.y);
    }
}

void Hero::jump()
{
    velocityY = jumpForce;
}
