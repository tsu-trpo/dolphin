#include "SelectorScene.h"
#include "Constants.h"
#include "DolphinSelectors.h"
#include "GameScene.h"
#include "Hero.h"
#include "MenuScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene *SelectorScene::createScene()
{
    return SelectorScene::create();
}

bool SelectorScene::init()
{
    if (!Scene::init()) {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    Vec2 shift = Vec2(300, 450);
    addButton(shift, file::texture::dolphinBlue, &selectDolphinBlue);

    shift = Vec2(512, 450);
    addButton(shift, file::texture::dolphinBlack, &selectDolphinBlack);

    shift = Vec2(724, 450);
    addButton(shift, file::texture::dolphinWhite, &selectDolphinWhite);

    auto background = Sprite::create(file::menu::menu);
    background->setPosition(Vec2{visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y});
    addChild(background, 0);

    auto label = Label::createWithTTF("D O L P H I N", file::font::ubuntu_bold, 50);
    label->setPosition(
        Vec2{origin.x + visibleSize.width / 2, origin.y + visibleSize.height - label->getContentSize().height});
    addChild(label, 1);

    return true;
}

void SelectorScene::addButton(Vec2 shift, const std::string &dolphin, const std::function<void()> selectDolphin)
{
    auto action = [this, selectDolphin](Ref *sender) {
        selectDolphin();
        this->goToGameScene(sender);
    };
    auto startItem = MenuItemImage::create(dolphin, dolphin, action);
    startItem->setPosition(shift);

    auto menu = Menu::create(startItem, NULL);
    menu->setPosition(Vec2::ZERO);
    addChild(menu, 1);
}

void SelectorScene::goToGameScene(Ref *sender)
{
    const float transitionTime = 0.5;
    auto scene = GameScene::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(transitionTime, scene));
}
