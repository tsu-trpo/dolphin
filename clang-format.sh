#!/bin/bash
set -e

format() {
    for src_file in $(find "$1" | egrep '\.(h|cpp)$'); do
        clang-format -i -style=file $src_file;
    done
}

sourcedir=$(dirname $(realpath $0))
sources="$sourcedir/Classes"

for i in ${sources[*]}; do
    format $i
done

echo "Done"
